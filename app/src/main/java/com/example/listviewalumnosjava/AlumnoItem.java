package com.example.listviewalumnosjava;
//Generación de la clase alumnoItem
public class AlumnoItem {
    private String nombre;
    private String matricula;
    private int imageID;

    public AlumnoItem(String nombre, String matricula, int imageID) {
        this.nombre = nombre;
        this.matricula = matricula;
        this.imageID = imageID;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setImageID(int imageID) {
        this.imageID = imageID;
    }

    public int getImageID() {
        return imageID;
    }
}
