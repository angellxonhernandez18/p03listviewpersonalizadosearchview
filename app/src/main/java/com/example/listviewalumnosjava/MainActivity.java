package com.example.listviewalumnosjava;
//Codificación del MainActvity
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import android.widget.SearchView;
import java.util.ArrayList;
import android.widget.SearchView.OnQueryTextListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private AlumnoAdapter adapter;
    private SearchView searchView;
    private ArrayList<AlumnoItem> originalList;
    private ArrayList<AlumnoItem> filteredList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<AlumnoItem> list = new ArrayList<>();

        String[] nombre = {
                getString(R.string.itemNombre1),
                getString(R.string.itemNombre2),
                getString(R.string.itemNombre3),
                getString(R.string.itemNombre4),
                getString(R.string.itemNombre5),
                getString(R.string.itemNombre6),
                getString(R.string.itemNombre7),
                getString(R.string.itemNombre8),
                getString(R.string.itemNombre9),
                getString(R.string.itemNombre10),
                getString(R.string.itemNombre11),
                getString(R.string.itemNombre12),
                getString(R.string.itemNombre13),
                getString(R.string.itemNombre14),
                getString(R.string.itemNombre15),
                getString(R.string.itemNombre16),
                getString(R.string.itemNombre17),
                getString(R.string.itemNombre18),
                getString(R.string.itemNombre19),
                getString(R.string.itemNombre20),
                getString(R.string.itemNombre21),
                getString(R.string.itemNombre22),
                getString(R.string.itemNombre23),
                getString(R.string.itemNombre24),
                getString(R.string.itemNombre25),
                getString(R.string.itemNombre26),
                getString(R.string.itemNombre27),
                getString(R.string.itemNombre28),
                getString(R.string.itemNombre29),
                getString(R.string.itemNombre30),
                getString(R.string.itemNombre31),
                getString(R.string.itemNombre32)
        };

        String[] matricula = {
                getString(R.string.msgMatricula1),
                getString(R.string.msgMatricula2),
                getString(R.string.msgMatricula3),
                getString(R.string.msgMatricula4),
                getString(R.string.msgMatricula5),
                getString(R.string.msgMatricula6),
                getString(R.string.msgMatricula7),
                getString(R.string.msgMatricula8),
                getString(R.string.msgMatricula9),
                getString(R.string.msgMatricula10),
                getString(R.string.msgMatricula11),
                getString(R.string.msgMatricula12),
                getString(R.string.msgMatricula13),
                getString(R.string.msgMatricula14),
                getString(R.string.msgMatricula15),
                getString(R.string.msgMatricula16),
                getString(R.string.msgMatricula17),
                getString(R.string.msgMatricula18),
                getString(R.string.msgMatricula19),
                getString(R.string.msgMatricula20),
                getString(R.string.msgMatricula21),
                getString(R.string.msgMatricula22),
                getString(R.string.msgMatricula23),
                getString(R.string.msgMatricula24),
                getString(R.string.msgMatricula25),
                getString(R.string.msgMatricula26),
                getString(R.string.msgMatricula27),
                getString(R.string.msgMatricula28),
                getString(R.string.msgMatricula29),
                getString(R.string.msgMatricula30),
                getString(R.string.msgMatricula31),
                getString(R.string.msgMatricula32)
        };

        int[] fotos = {
                R.drawable.tci2019030344,
                R.drawable.tci2020030174,
                R.drawable.tci2020030176,
                R.drawable.tci2020030181,
                R.drawable.tci2020030184,
                R.drawable.tci2020030189,
                R.drawable.tci2020030199,
                R.drawable.tci2020030212,
                R.drawable.tci2020030241,
                R.drawable.tci2020030243,
                R.drawable.tci2020030249,
                R.drawable.tci2020030264,
                R.drawable.tci2020030268,
                R.drawable.tci2020030292,
                R.drawable.tci2020030304,
                R.drawable.tci2020030306,
                R.drawable.tci2020030313,
                R.drawable.tci2020030315,
                R.drawable.tci2020030322,
                R.drawable.tci2020030325,
                R.drawable.tci2020030327,
                R.drawable.tci2020030329,
                R.drawable.tci2020030332,
                R.drawable.tci2020030333,
                R.drawable.tci2020030389,
                R.drawable.tci2020030766,
                R.drawable.tci2020030771,
                R.drawable.tci2020030777,
                R.drawable.tci2020030799,
                R.drawable.tci2020030808,
                R.drawable.tci2020030819,
                R.drawable.tci2020030865
        };

        for(int i = 0; i < 31; i++) {
            list.add(new AlumnoItem(nombre[i], matricula[i], fotos[i]));
        }

        listView = findViewById(R.id.lstAlumnos);
        AlumnoAdapter adapter = new AlumnoAdapter(this, R.layout.activity_alumno, list);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                String selectedCategory = list.get(position).getNombre();
                Toast.makeText(MainActivity.this, getString(R.string.msgSeleccionado) + " " + selectedCategory, Toast.LENGTH_SHORT).show();
            }


            public boolean onCreateOptionsMenu(Menu menu) {
                getMenuInflater().inflate(R.menu.searchview, menu);
                return true;
            }




        });
    }
}