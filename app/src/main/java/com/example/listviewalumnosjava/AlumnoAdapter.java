package com.example.listviewalumnosjava;
//Generación de la clase AdapterAlumno
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AlumnoAdapter extends ArrayAdapter<AlumnoItem> {
    private Context context;
    private int layoutResourceId;
    private ArrayList<AlumnoItem> data;

    public AlumnoAdapter(Context context, int layoutResourceId, ArrayList<AlumnoItem> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = LayoutInflater.from(context).inflate(layoutResourceId, parent, false);

        ImageView imagen = itemView.findViewById(R.id.imgFotoAlumno);
        imagen.setImageResource(data.get(position).getImageID());

        TextView textCategoria = itemView.findViewById(R.id.lblNombre);
        textCategoria.setText(data.get(position).getNombre());

        TextView textDescripcion = itemView.findViewById(R.id.lblMatricula);
        textDescripcion.setText(data.get(position).getMatricula());

        return itemView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getView(position, convertView, parent);
    }
}

